#Prototipo para leer humedad y temperatura#

### Como correr: ###
1-Instalar el software de arduino, descargable en la web official 


2-Agrege la librería dht11 a la carpeta de librerías de arduino en: 
"C:\Program Files (x86)\Arduino\libraries"

3-Correr el programa en la carpeta src, WeatherStation.ino con el software de arduino

4-Subir el codigo al arduino

5-Abrir el monitor serial en Herramientas


### Recursos ###

Tutorial original:
 http://www.hobbyist.co.nz/?q=documentations/wiring-up-dht11-temp-humidity-sensor-to-your-arduino

Datasheet:
http://www.micro4you.com/files/sensor/DHT11.pdf
